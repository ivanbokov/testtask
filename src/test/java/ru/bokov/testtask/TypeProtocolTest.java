package ru.bokov.testtask;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TypeProtocolTest {
    private TypeProtocol typeProtocol;
    private TypeProtocol typeProtocolCompare;
    @BeforeEach
    public void startTest(){
        List<Byte> message = new ArrayList<>(){{
            add((byte) 'A');
            add((byte) 5);
            add((byte) 'S');
            add((byte) 'u');
            add((byte) 'p');
            add((byte) 'e');
            add((byte) 'r');
        }};
        typeProtocol = TypeProtocolFactory.createTypeProtocol(message);
        message.set(0,(byte)'B');
        typeProtocolCompare = TypeProtocolFactory.createTypeProtocol(message);
    }

    @Test
    void testTypeClass(){
        assertTrue(typeProtocol instanceof TypeProtocolA);
        assertFalse(typeProtocolCompare instanceof TypeProtocolA);
    }
    @Test
    void testEquals(){
        assertTrue(typeProtocol.equals(typeProtocol));
        assertFalse(typeProtocol.equals(typeProtocolCompare));
    }

    @Test
    void testToString() {
        assertTrue(typeProtocol.toString().compareToIgnoreCase("[type: a, id=5, data=super]")==0);
        assertTrue(typeProtocolCompare.toString().compareToIgnoreCase("[type: b, id=5, data=super]")==0);
    }
}