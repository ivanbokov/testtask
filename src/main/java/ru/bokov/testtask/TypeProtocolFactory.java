package ru.bokov.testtask;

import java.util.List;

public class TypeProtocolFactory {//Фабрика создания объектов по сообщению
    static TypeProtocol createTypeProtocol(List<Byte> message){
        if(message.get(0) == 'A'){
            return new TypeProtocolA(message.get(1),message.subList(2,message.size()));
        }
        if(message.get(0) == 'B'){
            return new TypeProtocolB(message.get(1),message.subList(2,message.size()));
        }
        return null;
    }
 }
