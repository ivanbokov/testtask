package ru.bokov.testtask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;
//Сервер однопоточный
//Ограничение по ID задачи нумерация должна начинаться от 4, т.к. байты 2 и 3 заняты для ограничения сообщения на начало и конец
public class PoolServer {
    static int COUNT_THREAD = 10;//Количество потоков
    static Logger log = LoggerFactory.getLogger(PoolServer.class);//Получаем логгер и помещаем в статическую переменную для общего пользования
    static public void main(String[] args){
        int portServer = 3456;//Порт по умолчанию

        Socket client = null;
        Map<Class, Queue<TypeProtocol>> mapThreads = new ConcurrentHashMap<>();//Объект содержащий очереди задач
        Queue<Byte> bufferMessage = new LinkedList<>();//Объект выполняющий функцию буфера для получения сообщений от клиента
        List<Integer> runID = new CopyOnWriteArrayList<>();//Список ИД задач которые выполняются на данный момент
        ExecutorService executor = Executors.newFixedThreadPool(COUNT_THREAD);//Создадим объект управления потоками

        mapThreads.put(TypeProtocolA.class,new ConcurrentLinkedQueue<>());//Инициализируем наш объект хранения очоередей для типов сообщений
        mapThreads.put(TypeProtocolB.class,new ConcurrentLinkedQueue<>());

        if (args.length >= 1) {
            portServer = Integer.parseInt(args[0]);//Если нам передали агрумент то это порт. Установим его
        }
        PoolServer.log.info("Старт приложения");//Сообщим что начали работать

        try (ServerSocket server = new ServerSocket(portServer)){
            try {
                PoolServer.log.info("Сервер доступен по адресу:"+server.getLocalSocketAddress());

                while (true) {
                    client = server.accept();//Ждем соединения от клиента
                    InputStream inputServer = client.getInputStream();//Получаем входящий поток
                    int count;//Объем полученного сообщения
                    byte[] bytes = new byte[16*1024];//Буфер сообщения
                    while ((count = inputServer.read(bytes)) > 0) {//читаем сообщение от клиента
                        for(int i=0;i<count;i++)
                        {
                            bufferMessage.add(bytes[i]);//Помещаем в удобный буфер
                        }
                    }
                    Queue<TypeProtocol> queueType = parserMessage(bufferMessage);//Разбираем сообщения от клиента если он прислал сразу несколько и помещаем в очередь
                    while(!queueType.isEmpty()) {//Обрабатываем очередь пока она не станет пустой
                        TypeProtocol message = queueType.poll();//Получаем первый элемент очереди

                        mapThreads.get(message.getClass()).add(message);// Добавлем в очередь потоков
                        PoolServer.log.info("Размер очереди типа A: " + mapThreads.get(TypeProtocolA.class).size());//Сообщаем размеры очередей А
                        PoolServer.log.info("Размер очереди типа B: " + mapThreads.get(TypeProtocolB.class).size());//Сообщаем размеры очередей В
                        executor.execute(new Worker(mapThreads.get(message.getClass()),runID));// Отправляем на выполнение задачу
                    }

                    inputServer.close();//Закрываем входящий поток
                    client.close();//Закрываем соединение
                }
            }finally {
                if(!client.isClosed()) {//Если клиент не был закрыт до этого
                    client.close();//Закроем его
                }
                executor.shutdown();//Завершение выполнения потоков
            }
        }catch(IOException ex){
            log.error(ex.getMessage());
        }
    }
    private static Queue<TypeProtocol> parserMessage(Queue<Byte> queueMessage){//Функция разбора сообщений из потока байт
        Queue<Byte> buffer = new LinkedList<>();//Буфер задачи, инициализируем сразу, так как нет уверенности что первый байт будет стартовым
        Queue<TypeProtocol> queueType = new LinkedList<>();//Очередь задач которая вернется функцией
        while(!queueMessage.isEmpty()){//Пока есть байты для разбора-разбираем

            switch (queueMessage.peek()) {//Проверяем какой байт перед нами
                case 2:{//Если байт начальный, то пересоздаем буфер и сбрасываем этот байт
                    buffer = new LinkedList<>();
                    queueMessage.poll();
                    break;
                }
                case 3: {//Если байт закрывающий
                    if(buffer.size()>2) {//Если сообщение содержит больше 2 байт то оно корректно. 1 байт - Тип задачи, 2 байт - ИД задачи, 3 и последующие байты - данные задачи
                        queueType.add(TypeProtocolFactory.createTypeProtocol(Arrays.asList(buffer.toArray(new Byte[buffer.size()]))));//Создадим объект задачи с помощью фабрики
                    }
                    queueMessage.poll();//Сбросим байт
                    buffer.clear();//Очистим буферную очередь
                    break;
                }
                default: {
                    buffer.add(queueMessage.poll());//Собираем задачу по байтам
                    break;
                }
            }
        }
        if(!buffer.isEmpty()){//Если буфер не пустой то не дождались завершающего байта и вернем буфер в очередь. Возможно после прийдут еще байты от клиента
            queueMessage.clear();//Очищаем очередь
            queueMessage.add((byte)2);//Добавляем начальный байт в очередь
            queueMessage.addAll(buffer);//Помещаем накопленный буфер
        }
        return queueType;
    }
}
