package ru.bokov.testtask;

import java.util.List;

public class TypeProtocolB extends TypeProtocol{//Класс сообщений В
    public TypeProtocolB(int id, String data) {
        super("B", id, data);
    }
    public TypeProtocolB(int id, List<Byte> data) {
        super("B", id, data);
    }
}
