package ru.bokov.testtask;

import java.util.List;

public class TypeProtocolA extends TypeProtocol{//Класс для сообщений А

    public TypeProtocolA(int id, String data) {
        super("A", id, data);
    }

    public TypeProtocolA(int id, List<Byte> data) {
        super("A", id, data);
    }
}
