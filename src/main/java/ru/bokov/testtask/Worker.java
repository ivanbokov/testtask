package ru.bokov.testtask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Worker implements Runnable{
    TypeProtocol job;//Задача выполнения
    Queue<TypeProtocol> jobQueue;//Очередь задач
    List<Integer> runID;//Список ИД задач которые выполняются в данный момент
    Worker(Queue<TypeProtocol> typeProtocol, List<Integer> runID){
        this.jobQueue = typeProtocol;
        this.runID = runID;
    }

    @Override
    public void run() {
        if ((job = jobQueue.poll()) != null) {//Получаем задачу на выполнение из очереди
            while (true) {
                try {
                    synchronized (runID) {//Заблокируем объект между потоками и проверим его на наличие выполнения задачи с таким же ИД
                        if (!runID.contains(job.getID())) {//Если не нашли
                            runID.add(job.getID());//Добавим
                            break;//Выйдем из цикла
                        }
                    }
                    TimeUnit.MILLISECONDS.sleep(1);//Немного подождем чтобы не злоупотреблять ресурсами
                } catch (InterruptedException ex) {
                    PoolServer.log.error(ex.getStackTrace().toString());//Обработка ошибки
                    break;
                }
            }
            executeJob();//Если мы здесь то мы поместили ИД в список выполнения и можем выполнять задачу
            synchronized (runID) {//Заблокируем объект на удаление
                runID.remove((Integer) job.getID());//Удалим ИД после выполнения
            }
        }
    }
    private void executeJob(){//Процедура выполнения задачи
        Random r=new Random();
        PoolServer.log.info(String.format("Старт: %s, в очереди запросов: %d", job.toString(),jobQueue.size()));//Выводим сообщение что начали выполнять задачу
        try {
            TimeUnit.MILLISECONDS.sleep(r.nextInt(100));//Засыпаем на случайное число времени от 0 до 99 милисекунд. Имитируем работу задачи не равномерно
        } catch (InterruptedException e) {
            PoolServer.log.error(e.getStackTrace().toString());//Если возникла ошибка
        }
        PoolServer.log.info(String.format("Окончание: %s, в очереди запросов: %d", job.toString(),jobQueue.size()));//Рапортуем что закончиили работу
    }
}
