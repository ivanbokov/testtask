package ru.bokov.testtask;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Random;

public class ClientPool {
    static public void main(String[] args){
        Random r=new Random();
        try ( Socket clientSocket = new Socket("127.0.0.1",3456)){
            OutputStream out =clientSocket.getOutputStream();
            for(int i=0;i<100;i++){
                out.write(message(r.nextInt(2),(byte)r.nextInt(11),"message("+r.nextInt(1000)+")"));
            }
            out.close();
        }catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }
    static private byte[] message(int type,byte id,String data){
        System.out.println("Type: "+type+" id="+id+" data="+data);
        byte[] m = new byte[4+data.length()];
        m[0]=2;
        if(type==0){
            m[1] = 'A';
        }else{
            m[1] = 'B';
        }
        m[2] = (byte) (id+4);
        int j=2;
        for(byte c:data.getBytes(StandardCharsets.UTF_8)){
            m[++j] = c;
        }
        m[++j] = 3;
        return m;
    }
}
