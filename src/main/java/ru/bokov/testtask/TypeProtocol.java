package ru.bokov.testtask;

import java.util.List;

public abstract class TypeProtocol{//Абстрактный класс протокола. Все методы реализованы унифицированно
    private int ID;
    private String name;
    private String data;

    public String getName(){
        return name;
    }
    public int getID(){
        return ID;
    }
    public String getData(){ return data; }

    @Override
    public int hashCode() {//Переписываем генерацию Hash кода под наш объект
        int result = 15;
        result = 32 * result + name.hashCode();
        result = 32 * result + ID;
        result = 32 * result + data.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {//Своя реализация сравнения по нашим полям объекта.
        if(obj == this){//Если объект он сам
            return true;
        }
        if(!(obj instanceof TypeProtocol)){//Если пытаемся сравнивать с другим классом
            return false;
        }
        TypeProtocol typeProtocol = (TypeProtocol) obj;
        return ID==typeProtocol.getID()
                && name.compareToIgnoreCase(typeProtocol.getName())==0
                && data.compareToIgnoreCase(typeProtocol.getData())==0;
    }

    public TypeProtocol(String name, int id, String data){
        this.ID = id;
        this.data = data;
        this.name = name;
    }
    public TypeProtocol(String name, int id, List<Byte> data){
        this.ID = id;
        StringBuilder stringBuilder = new StringBuilder();
        for(byte b:data){
            stringBuilder.append((char) b);
        }
        this.data = stringBuilder.toString();
        this.name = name;
    }

    @Override
    public String toString() {
        return "[Type: "+name+", ID="+ID+", data="+data+"]";
    }
}
